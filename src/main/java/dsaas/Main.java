package za.co.pbit.dsaas;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import java.net.URISyntaxException;

import java.util.Scanner;
import java.io.File;

class User {
	private String name, surname;
	private int age;

	public User () {}

	public User (String name, String surname, int age) {
		this.name = name;
		this.surname = surname;
		this.age = age;
	}

	@Override
	public String toString() {
		return "User: " + name + " " + surname + " "  + age;
	}
}

class Action {
	String action;

	public Action(String action) {
		this.action = action;
	}

	@Override
	public String toString() {
		return this.action;
	}
}

public class Main {
	public static final boolean VERBOSE = false;

	public static void main(String [] args) {

		String namespace = "pierre";
		String id = "test";
		VersionControl<MapCloudStructure> mapVersionControl = new CloudStructureFactory<MapCloudStructure>(namespace, id, MapCloudStructure.class, User.class).getVersionControl();
		MapCloudStructure<User> cloudmap = mapVersionControl.initialVersion();
		
		long time = System.currentTimeMillis();
		cloudmap.put("kaas", new User("kaas", "sie", 1));

		System.out.println(System.currentTimeMillis() - time);
		cloudmap.close();

		// CloudStructureFactory<GraphCloudStructure> graphFact = new CloudStructureFactory<GraphCloudStructure>( namespace, id, GraphCloudStructure.class, User.class, String.class);
		// VersionControl<GraphCloudStructure> graphVC = graphFact.getVersionControl();
		// GraphCloudStructure<User, String> graph = graphVC.initialVersion();

		// graph.addVertex("node1", new User("Peter", "Pan", 11));
		// graph.addVertex("node2", new User("Alice", "A", 30));
		// graph.addVertex("node3", new User("Ben", "Little", 32));
		// graph.addEdge("node1", "node2", "node2Connector");
		// graph.addEdge("node1", "node3", "node3Connector");

		// for (String edge : graph.getEdgesOfVertex("node1")) {
		// 	System.out.println(edge);
		// }


		// CloudStructureFactory<GraphCloudStructure> myGraphFactory = new CloudStructureFactory<GraphCloudStructure>( namespace, "graphtest", GraphCloudStructure.class, String.class, String.class);
		// VersionControl<GraphCloudStructure> graphVersionControl = myGraphFactory.getVersionControl();
		// graph = graphVersionControl.initialVersion();

		// String node1Id = graph.addVertex("id1","Node 1");
		// String node2Id = graph.addVertex("id2","Node 2");
		
		// graph.updateVertex("id2", "Node 2 Updated");
		
		// String node3Id = graph.addVertex("id3", "Node 3");
		// graph.updateVertex(node3Id, "Node 3 updated");
		// graph.removeVertex(node3Id);

		// graph.addEdge(node1Id, node2Id, "Connecting edge");
		// System.out.println(graph.getEdge(node1Id, node2Id));
		// System.out.println(graph.getVersion());

		// myGraphFactory = new CloudStructureFactory<GraphCloudStructure>(namespace, "user", GraphCloudStructure.class, User.class, Action.class);
		// VersionControl<GraphCloudStructure> gvc = myGraphFactory.getVersionControl();
		// GraphCloudStructure<User, Action> graph2 = gvc.initialVersion();
		// graph2.addVertex("peter", new User("Peter", "Pan", 1));
		// graph2.addVertex("alice", new User("Alice", "A", 3));
		// graph2.addEdge("alice", "peter", new Action("kills"));

		// Action action = graph2.getEdge("alice", "peter");
		// System.out.println(action);
		// System.out.println(graph2.getEdge("peter", "alice"));

		// System.out.println("Adjacent to peter:");
		// for (User user : graph2.adjacent("peter")) {
		// 	System.out.println(user);
		// }

		// System.out.println("Parents of peter:");
		// for (User user : graph2.parents("peter")) {
		// 	System.out.println(user);
		// }

		// System.out.println("Children of alice:");
		// for (User user : graph2.children("alice")) {
		// 	System.out.println(user);
		// }

	}

	private static void print(Collection<?> elements) {
		for (Object element : elements) {
			System.out.println(element);
		}
	}
}