package za.co.pbit.dsaas;

public class EntryWrapper<V> {
	public String key;
	public V value;

	public EntryWrapper(String key, V value) {
		this.key = key;
		this.value = value;
	}
}