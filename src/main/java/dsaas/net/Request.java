package za.co.pbit.dsaas;

import com.google.gson.JsonParser;
import io.socket.client.Ack;

public abstract class Request {

	protected Auth auth;
	protected final String USER_AGENT = "DSaaS-Java-Client/1.0";
	// protected final String HOST = "http://dsaas.pbit.co.za";
	protected final String HOST = "http://localhost:3000";
	protected JsonParser parser = new JsonParser();
	protected boolean log = false;

	public abstract String gson(String method, String url, String value);
	public abstract void emit(String event, Object[] args, Ack ack);
	public abstract void close();
}