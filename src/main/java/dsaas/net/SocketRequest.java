package za.co.pbit.dsaas;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.client.Ack;

public class SocketRequest extends Request {

	private Socket socket;

	public SocketRequest(Auth auth) {
		this.auth = auth;
		try {
			this.socket = IO.socket(HOST);
			this.socket.connect();
		} catch (Exception u) {
			u.printStackTrace();
			throw new RuntimeException();
		}
	}

	public SocketRequest(Auth auth, boolean log) {
		this(auth);
		this.log = log;
	}

	@Override
	public String gson(String method, String url, String value) {
		/* Do Nothing */
		return "";
	}

	@Override
	public void emit(String event, Object[] args, Ack ack) {
		System.out.println("Sending");
		this.socket.emit(event, args, ack);
	}

	@Override
	public void close() {
		this.socket.close();
	}
}