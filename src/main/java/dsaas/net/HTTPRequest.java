package za.co.pbit.dsaas;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.JsonObject;
import com.google.gson.JsonElement;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Iterator;
import java.nio.charset.StandardCharsets;
import java.lang.IllegalStateException;
import java.lang.ClassCastException;
import java.lang.RuntimeException;
import io.socket.client.Ack;


public class HTTPRequest extends Request {

	public HTTPRequest(Auth auth) {
		this.auth = auth;
	}

	public HTTPRequest(Auth auth, boolean log) {
		this(auth);
		this.log = log;
	}

	@Override
	public String gson(String method, String url, String value) {
		double start = System.currentTimeMillis();

		method = method.toUpperCase();
		HttpURLConnection conn = createConnection(url, method);

		if (value != null) {
			try {
				parser.parse(value);
				if (value.charAt(0) != '{') throw new JsonSyntaxException("Cannot parse.");
			} catch (JsonSyntaxException s) {
				// If the value is not valid Json, then create a valid
				// json object and store it as #value
				JsonObject object = new JsonObject();
				object.addProperty("#value", value);
				value = object.toString();
			}
			send(conn, value);	
		} 
		
		String response = "";
		try {
			response = getText(conn);
		} catch(IOException io) {
			System.out.println("IOException: " + io.getMessage());
		}

		JsonElement el;
		try {
			el = parser.parse(response);
			JsonObject object = el.getAsJsonObject();
			if (object.has("#value")) {
				response = object.get("#value").getAsString();
			}
		} catch (JsonSyntaxException s) {}
		  catch (IllegalStateException e) {}

		if (log) {
			System.out.println("Time: " + (System.currentTimeMillis() - start));
		}
		return response;
	}

	@Override
	public void close() {
		/* Do Nothing */
	}

	@Override
	public void emit(String event, Object[] args, Ack ack) {
	}

	private HttpURLConnection createConnection(String url, String method) {
		HttpURLConnection conn = null;
		try {
			URL obj = new URL(HOST + url);
			conn = (HttpURLConnection) obj.openConnection();
			// Request Headers
			conn.setRequestMethod(method);
			if (this.auth != null && this.auth.getToken() != "") {
				conn.setRequestProperty("Authorization", auth.getToken());
			}
			
		} catch (IOException io) {
			System.out.println("createConnection " + io);
		}

		return conn;
	}

	private void send(HttpURLConnection conn, String param) {
		conn.setRequestProperty( "Content-Type", "application/json");
		doOutput(conn, param);
	}

	private void doOutput(HttpURLConnection conn, String toSend) {
		byte[] postData = toSend.getBytes( StandardCharsets.UTF_8 );
		int postDataLength = postData.length;
		conn.setDoOutput( true );
		conn.setRequestProperty( "charset", "utf-8");
		conn.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));

		try {
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
			wr.write( postData );
		} catch (IOException e) {}

	}

	private String getText(HttpURLConnection conn) throws IOException {
		String out = "";
		int responseCode = conn.getResponseCode();
		if (responseCode != 200) {
			if (responseCode == 401) {
				throw new RuntimeException("Your authorization token is incorrect, please change it.");
			} else {
				System.out.println(responseCode);
			}
		}

		BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		out = response.toString();
		return out;
	}
}