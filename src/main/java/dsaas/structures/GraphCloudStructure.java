package za.co.pbit.dsaas;

import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeMap;
import java.util.Map;
import java.lang.reflect.Type;
import java.lang.RuntimeException;

import org.jgrapht.Graph;
import org.jgrapht.EdgeFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonElement;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonParseException;
import com.google.gson.JsonDeserializer;
import com.google.gson.reflect.TypeToken;


class NodeContainer<V> {
    public String id;
    public Collection<String> edges;
    public V props;

    public NodeContainer(String id, Collection<String> edges, V props) {
        this.id = id;
        this.edges = edges;
        this.props = props;
    }
}


class EdgeNodeMatcher {
    public String startVertexId;
    public String endVertexId;

    public EdgeNodeMatcher(String startVertexId, String endVertexId) {
        this.startVertexId = startVertexId;
        this.endVertexId = endVertexId;
    }
}


class NodeAdapter<V> implements JsonDeserializer<NodeContainer<V>> {
    private Class<V> vertexClass;
    public NodeAdapter(Class<V> vertexClass) {
        super();
        this.vertexClass = vertexClass;
    }

    public NodeContainer<V> deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context)
        throws JsonParseException {

        JsonObject obj = jsonElement.getAsJsonObject();
        V vertex = context.deserialize(obj.get("props"), vertexClass);
        Map<String, Integer> edges = context.deserialize(obj.get("edges"), new TypeToken<Map<String, Integer>>() {}.getType());
        return new NodeContainer<V>(obj.get("id").getAsString(), edges.keySet(), vertex);
    }
}


public class GraphCloudStructure<V, E> {
	
	private Request request;
	private String preamble, postamble, version;
	private boolean graphRetrieved;
	private Gson gson;
	private Type nodeType, edgeType;
    private Class<V> vertexClass;
    private Class<E> edgeClass;

	private Map<String, NodeContainer<V>> cacheVertices;
	private Map<String, E> cacheEdges;
    private Map<String, EdgeNodeMatcher> cacheEdgeToVertices;

	public GraphCloudStructure(Auth auth, String namespace, String id, String version, Class<V> vertexClass, Class<E> edgeClass) {
		this.request = new HTTPRequest(auth, Main.VERBOSE);
		this.version = version;
		this.preamble = "/api/" + namespace + "/" + id + "/";
		this.postamble = "/graph/";
		this.gson = new GsonBuilder().registerTypeAdapter(NodeContainer.class, new NodeAdapter<V>(vertexClass)).create();
		this.nodeType = new TypeToken<V>() {}.getType();
		this.edgeType = new TypeToken<E>() {}.getType();

        this.vertexClass = vertexClass;
        this.edgeClass = edgeClass;

		// A local copy of the nodes and edges
		this.cacheVertices = new TreeMap<String, NodeContainer<V>>();
		this.cacheEdges = new TreeMap<String, E>();
        this.cacheEdgeToVertices = new TreeMap<String, EdgeNodeMatcher>();
        this.getGraph();
	}

	private String url() {
		return this.preamble + this.version + this.postamble;
	}

    public void getGraph() {
        if (graphRetrieved) return;
        graphRetrieved = true;
        Type listType = new TypeToken<List<String>>() {}.getType();
        List<String> nodeIdentifiers = gson.fromJson(request.gson("get", this.url() + "nodes", null), listType);
        for (String id : nodeIdentifiers) {
            if (!this.cacheVertices.containsKey(id)) {
                NodeContainer<V> nodeContainer = this.getNodeContainer(id);
                cacheVertices.put(id, nodeContainer);

                // for (String edgeId : nodeContainer.edges) {
                //     if (!cacehEdges.containsKey(edgeId)) {
                //         EdgeContainer<E> edgeContainer = this.getEdgeContainer(id);
                //         cacehEdges.put(id, edgeContainer.edge);
                //     }
                // }
            }
        }

    }

    // VERTEX

    public String addVertex(V v) {
        return this.addVertex(null, v);
    }

    public String addVertex(String id, V v) {
        if (id != null && !id.equals("")) {
            id = "/" + id;
        } else {
            id = "";
        }
        String response = request.gson("post", this.url() + "node" + id, gson.toJson(v));
        JsonObject obj = gson.fromJson(response, JsonObject.class);
        if (obj.get("error") != null) {
            throw new RuntimeException(obj.get("error").getAsString());
        }
        String identifier = obj.get("id").getAsString();
        this.cacheVertices.put(identifier, new NodeContainer(identifier, null, v));
        this.version = obj.get("version").getAsString();
        return identifier;
    }

    private NodeContainer<V> getNodeContainer(String id) {
        String response = request.gson("get", this.url() + "node/" + id, null);
        return gson.fromJson(response, new TypeToken<NodeContainer<V>>() {}.getType());
    }

    public V getVertex(String id) {
        // if (this.cacheVertices.containsKey(id)) {
        //     return this.cacheVertices.get(id);
        // }
        NodeContainer<V> nodeContainer = getNodeContainer(id);
        this.cacheVertices.put(id, nodeContainer);
        if (nodeContainer == null) return null;
        return nodeContainer.props;
    }

    public Collection<E> getEdgesOfVertex(String vertexId) {
        ArrayList<E> edges = new ArrayList<E>();
        NodeContainer<V> nodeContainer = getNodeContainer(vertexId);
        for (String edgeId : nodeContainer.edges) {
            edges.add(this.getEdge(edgeId));            
        }
        return edges;
    }

    public void updateVertex(String id, V v) {
        String url = this.url() + "node/" + id;
        if (!this.cacheVertices.containsKey(id)) {
            throw new RuntimeException("The vertex does not exist locally, please make sure it exists and make sure you have a local copy.");
        }
        JsonObject obj = gson.fromJson(request.gson("put", url, gson.toJson(v)), JsonObject.class);
        this.cacheVertices.get(id).props = v;
        this.version = obj.get("version").getAsString();
    }

    public void removeVertex(String id) {
        JsonObject obj = gson.fromJson(request.gson("delete", this.url() + "node/" + id, null), JsonObject.class);
        this.cacheVertices.remove(id);
        this.version = obj.get("version").getAsString();
    }

    public Collection<V> adjacent(String id) {
        Map<String, NodeContainer<V>> obj = gson.fromJson(request.gson("get", this.url() + id + "/adjacent", null), new TypeToken<Map<String, NodeContainer<V>>>(){}.getType());
        return getCollection(obj);
    }

    public Collection<V> parents(String id) {
        Map<String, NodeContainer<V>> obj = gson.fromJson(request.gson("get", this.url() + id + "/parents", null), new TypeToken<Map<String, NodeContainer<V>>>(){}.getType());
        return getCollection(obj);
    }

    public Collection<V> children(String id) {
        Map<String, NodeContainer<V>> obj = gson.fromJson(request.gson("get", this.url() + id + "/children", null), new TypeToken<Map<String, NodeContainer<V>>>(){}.getType());
        return getCollection(obj);
    }

    public Collection<V> getCollection(Map<String, NodeContainer<V>> map) {
        ArrayList<V> collectionOfNodes = new ArrayList<V>();
        for (NodeContainer<V> nodeContainer : map.values()) {
            collectionOfNodes.add(nodeContainer.props);
        }
        return collectionOfNodes;
    }

    // EDGE

    public String addEdge(String startVertex, String endVertex, E properties) {
        JsonObject obj = gson.fromJson(request.gson("post", this.url() + "edge/" +  startVertex + "/" + endVertex, gson.toJson(properties)), JsonObject.class);
        String identifier = obj.get("id").getAsString();

        this.cacheEdgeToVertices.put(identifier, new EdgeNodeMatcher(startVertex, endVertex));
        this.cacheEdges.put(identifier, properties);
        this.version = obj.get("version").getAsString();

        return identifier;
    }

    public void updateEdge(String edgeId, E properties) {
        JsonObject obj = gson.fromJson(request.gson("put", this.url() + "edge/" + edgeId, gson.toJson(properties)), JsonObject.class);
        this.cacheEdges.put(edgeId, properties);
        this.version = obj.get("version").getAsString();
    }

    public E getEdge(String id) {
        return gson.fromJson(request.gson("get", this.url() + "edge/" + id, null), edgeClass);
    }

    public E getEdge(String startVertex, String endVertex) {
        return gson.fromJson(request.gson("get", this.url() + "edge/" + startVertex + "/" + endVertex, null), edgeClass);
    }

    // GENERAL

    public Collection<V> getVertices() {
        this.getGraph();
        ArrayList<V> vertices = new ArrayList<V>();
        for (NodeContainer container : this.cacheVertices.values()) {
            vertices.add((V) container.props);
        }
        return vertices;
    }

    public Collection<E> getEdges() {
        this.getGraph();
        return this.cacheEdges.values();
    }

    public String getVersion() {
        return this.version;
    }

}