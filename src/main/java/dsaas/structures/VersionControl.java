package za.co.pbit.dsaas;

import java.util.TreeMap;
import java.util.Map;
import java.util.List;
import java.util.Iterator;
import java.util.LinkedList;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.JsonObject;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonNull;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.InstantiationException;


public class VersionControl<T> {

	private Request request;
	private Map<String, String[]> map;
	private Auth auth;
	private String namespace, id;
	private Gson gson;
	private Constructor<T> ctor = null;
	private Class<?>[] classes;

	private final int LIMIT = 2;

	public VersionControl(Auth auth, String namespace, String id, Class<T> clazz, Class<?>... classes) {
		this.request = new HTTPRequest(auth);
		this.namespace = namespace;
		this.id = id;
		this.auth = auth;
		this.gson = new Gson();

		int tried = -1;
		out:while (tried < LIMIT) {
			try {
				tried++;
				switch (tried) {
					case 0:
						this.ctor = clazz.getConstructor(Auth.class, String.class, String.class, String.class, Class.class);
						break out;
					case 1:
						this.ctor = clazz.getConstructor(Auth.class, String.class, String.class, String.class, Class.class, Class.class);
						break out;
				}
			} catch (NoSuchMethodException m) {}
		}

		this.classes = classes;

		if (tried == LIMIT) {
			System.out.println("Cannot create with this Version Control");
		}

		if (Main.VERBOSE) System.out.println("Retrieving the version control for " + namespace + " / " + id);
		Type type = new TypeToken<Map<String, List<String>>>() {}.getType();
		String response = request.gson("get", "/api/" + namespace + "/" + id + "/history?level=1", null);
		map = gson.fromJson(response, type);
	}

	public T initialVersion() {
		return get("0");
	}

	public T get(String version) {
		for (int i = 0; i < LIMIT; i++) {
			try {
				switch(i) {
					case 0:
						return ctor.newInstance(auth, namespace, id, version, classes[0]);
					case 1:
						return ctor.newInstance(auth, namespace, id, version, classes[0], classes[1]);
				}
			} catch (InstantiationException ie) {
				System.out.println("Cannot create new instance of weird type.");
			} catch (IllegalAccessException e) {
				System.out.println(e);
			} catch (InvocationTargetException e) {
				System.out.println(e.getCause());
			} catch (IllegalArgumentException e) {}
		}
		return null;
	}

	public T merge(String versionA, String versionB) {
		TreeMap<String, String> parameters = new TreeMap<String, String>();
		parameters.put("versionA", versionA);
		parameters.put("versionB", versionB);
		JsonObject obj = gson.fromJson(request.gson("put", "/api/" + namespace + "/" + id + "/merge", gson.toJson(parameters)), JsonObject.class);
		return get(obj.get("version").getAsString());
	}

	public String getVersionFromTag(String tag) {
		String response = request.gson("get", "/api/" + namespace + "/" + id + "/tag/" + tag, null);
		JsonObject obj = gson.fromJson(response, JsonObject.class);
		if (obj == null || obj.get("version").equals(JsonNull.INSTANCE)) return null;
		return obj.get("version").getAsString();
	}

	public void addTag(String tag, String version) {
		TreeMap<String, String> parameters = new TreeMap<String, String>();
		parameters.put("version", version);
		request.gson("post", "/api/" + namespace + "/" + id + "/tag/" + tag, gson.toJson(parameters));
	}
}