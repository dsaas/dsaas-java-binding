package za.co.pbit.dsaas;
/**
 * The factory for creating DSaaS structures.
 */
public class CloudStructureFactory<T> {

	private Auth auth;
	private String namespace, id;
	private Class<T> dataStructureClass;
	private Class<?>[] classes;

	public CloudStructureFactory(String namespace, String id, Class<T> dataStructureClass, Class<?>... classes) {
		this.auth = Auth.setup();
		this.namespace = namespace;
		this.id = id;
		this.dataStructureClass = dataStructureClass;
		this.classes = classes;
	}

	public VersionControl<T> getVersionControl() {
		return new VersionControl<T>(auth, namespace, id, dataStructureClass, classes);
	}
}