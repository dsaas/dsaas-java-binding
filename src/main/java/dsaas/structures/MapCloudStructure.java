package za.co.pbit.dsaas;

import java.net.HttpURLConnection;
import java.util.Map;
import java.util.Set;
import java.util.List;
import java.util.Collection;
import java.util.ArrayList;
import java.util.TreeSet;
import java.util.HashMap;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.lang.RuntimeException;
import io.socket.client.Ack;

public class MapCloudStructure<V> implements Map<String, V> {

	private Request request;
	private String version;
	private String preamble, postamble;
	private String namespace, id;
	private Map<String, V> cacheMap;
	private boolean keysRetrieved, bothRetrieved;
	private Gson gson;
	private Type valueType;
	private Class<V> clazz;
	// private WebWorker webWorker;

	public MapCloudStructure(Auth auth, String namespace, String id, String version, Class<V> clazz) {
		this.request = new SocketRequest(auth, Main.VERBOSE);
		this.version = version;
		this.preamble = "/api/"+namespace+"/"+id+"/";
		this.namespace = namespace;
		this.id = id;
		this.postamble = "/map/";
		this.cacheMap = new HashMap<String, V>();
		this.gson = new Gson();
		this.clazz = clazz;
		this.lock = false;

		// this.webWorker = new WebWorker();
		// this.webWorker.start();
		// this.webWorker.on()
	}

	public void close() {
		// this.webWorker.stop();
	}

	private String url() {
		return preamble + version + postamble;
	}

	@Override
	public Set<Map.Entry<String, V>> entrySet() {
		Set<String> keys = this.keySet();
		for (String key : keys) {
			if (cacheMap.get(key) == null) {
				this.get(key);
			}
		}
		return cacheMap.entrySet();
	}

	@Override
	public Collection<V> values() {
		entrySet();
		return cacheMap.values();
	}

	@Override
	public Set<String> keySet() {
		if (keysRetrieved || bothRetrieved) return cacheMap.keySet();

		Type type = new TypeToken<List<String>>() {}.getType();
		ArrayList<String> keys = gson.fromJson(request.gson("get", this.url() + "keys", null), type);
		for (String key : keys) {
			if (!cacheMap.containsKey(key))
				cacheMap.put(key, null);
		}
		keysRetrieved = true;
		return cacheMap.keySet();
	}

	public void clear() {
		throw new RuntimeException("Unsupported, should we support this?");
	}

	public void putAll(Map map) {
		throw new RuntimeException("Unsupported, should we support this?");
	}

	public V remove(Object key) {
		V object = null;
		// Get the local object
		if (cacheMap.containsKey(key))
			object = cacheMap.remove(key);

		// If no local object was found, get the online object
		if (object == null) object = gson.fromJson(request.gson("get", this.url() + key, null), clazz);

		// Remove the object from the server
		JsonObject json = gson.fromJson(request.gson("delete", this.url() + key, null), JsonObject.class);
		this.version = json.get("version").getAsString();
		return object;
	}

	@Override
	public V put(String key, V v) {
		Object[] args = new Object[] { 
			this.namespace,
			this.id, 
			this.version,
			this.nextVersion,
			key,
			gson.toJson(v) 
		};

		request.emit("/api/map:put", args, new Ack() {
				@Override
				public void call(Object... args) {
					System.out.println("receiving ack");
					for (Object a : args) {
						System.out.println(a);
						System.out.println(a instanceof JsonObject);
					}
					// version = json.get("version").getAsString();
				}
			});

		this.version = a
		cacheMap.put((String) key, (V) v);
		// JsonObject json = gson.fromJson(request.gson("post", this.url() + key, gson.toJson(v)), JsonObject.class);
		return v;
	}

	@Override
	public V get(Object k) {
		String key = (String) k;
		V obj = null;
		if (cacheMap.containsKey(key))
			obj = cacheMap.get(key);

		if (obj == null) {
			return gson.fromJson(request.gson("get", this.url() + key, null), clazz);
		} else {
			return obj;
		}
	}

	public boolean containsValue(Object v) {
		return this.values().contains(v);
	}

	public boolean containsKey(Object k) {
		return this.keySet().contains(k);
	}

	public boolean isEmpty() {
		return this.size() == 0;
	}

	public int size() {
		return this.keySet().size();
	}

	public String getVersion() {
		return this.version;
	}
}