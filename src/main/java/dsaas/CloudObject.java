package za.co.pbit.dsaas;

public class CloudObject<V> {
	private String id;
	private V value;

	public CloudObject(V value) {
		this.value = value;
	}

	public CloudObject(String id, V value) {
		this.value =  value;
		this.id = id;
	}

	public void setID(String id) { this.id = id; }
	public String getID() { return this.id; }
	public void setValue(V value) { this.value = value; }
	public V getValue() { return this.value; }
}