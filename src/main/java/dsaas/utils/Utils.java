package za.co.pbit.dsaas;

import java.lang.reflect.Type;
import com.google.gson.Gson;

public class Utils {
	private static Gson gson = new Gson();

	public static <T> T toType(Object obj, Class<T> classOfT) {
		return gson.fromJson((String) obj, classOfT);
	}

	public static <T> T toType(Object obj, Type typeOfT) {
		return gson.fromJson((String) obj, typeOfT);
	}
}