package za.co.pbit.dsaas;

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

/** 
 * For authenticating the requests.
 */
public class Auth {
	
	private String token;

	public Auth(String token) {
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	public static Auth setup() {
		// Look for a text file named 'token.txt'
		// Read that text file and set up the system
		String fileName = "auth.txt";
		String dirName = System.getProperty("user.dir");
		
		File found = explore(new File(dirName));

		if (found != null && found.exists()) {
			try {
				Scanner sc = new Scanner(found);
				String token = sc.next();
				sc.close();
				return new Auth(token);
			} catch (FileNotFoundException f) {
				throw new RuntimeException("File not found???");
			}
		} else {
			System.out.println("File named auth.txt was not found in working directory.");
		}
		return new Auth("");
	}

	private static File explore(File dir) {
		for (File f : dir.listFiles()) {

			if (f.isFile() && f.getName().equals("auth.txt")) {
				return f;
			} else if (f.isDirectory()) {
				File found = explore(f);
				if (found != null) return found;
			}
		}
		return null;
	}
}