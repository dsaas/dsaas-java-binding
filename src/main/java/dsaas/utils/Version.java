package za.co.pbit.dsaas;

public class Version {
	private String temp = "";
	private String real = "";

	public Version(String temp) {
		this.temp = temp;
	}

	public String getTemp() { return temp; }
	public String getReal() { return real; }

	public void setTemp(String temp) { this.temp = temp; }
	public void setReal(String real) { this.real = real; }
}